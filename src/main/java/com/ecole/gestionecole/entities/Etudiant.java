package com.ecole.gestionecole.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Etudiant {
    private Long id;
    private String firstName;
    private String lastName;
    private Date dateOfBirth;
}
